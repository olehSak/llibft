#include <stdio.h>
#include "libft.h"
#include <string.h>
#include <ctype.h>

//      BZERO_TEST  =   str memory to '\0'
void    ft_bzero_test()
{
    char ar[10] = "str111";
    ft_bzero(ar, 3);
}

//      MEMSET_TEST =   set mem to
void    ft_memset_test()
{
    char ar[5] = "str1";
    ft_memset(ar, 3, 5);
}

//      STRCPY_TEST =   copy memory (destination/source)
void    ft_strcpy_test()
{
    char str1[10] = "perfect";
    char str2[10];
    printf("\nstrcpy result: %s", ft_strcpy(str2, str1));
}

//      STRLEN_TEST = lengths of string
void    ft_strlen_test()
{
    char str1[10] = "strlen";
    printf("\nstrlen result: %zu", ft_strlen(str1));
}

void    ft_strdup_test()
{
    char    str[11] = "osen'vesna";
    char    *istr;
    char    *sistr;
    istr = ft_strdup(str);
    sistr = strdup(str);
    printf("\nstrdup result: %s", istr);
    printf("\nstd strdup result: %s", sistr);
}

//      MEMCPY_TEST =   copy memory to new str
void    ft_memcpy_test()
{
    char str1[5] = "NULL";
    char str2[5] = "NULL";
    printf("\nmemcpy result: %p", ft_memcpy(NULL, NULL, 2));
}

void    ft_putstr_test()
{
    char str[10] = "@putstr@";
    printf("\nputstr result: \n");
    ft_putstr(str);
}

void    ft_strcat_test()
{
    char str[100] = "unit ";
    char str1[10] = "test.@";
    ft_strcat(str, str1);
    printf("\nstrcat result: %s", str);
  //  strcat(str, str1);
 //   printf("strcat: %s", str);
}

void    ft_strncat_test()
{
    // Массивы в которые добавляется строка
    char src1[10]="000";
    char src2[10]="000";
    // Добавляемая строка
    char app[10]="12345";

    // Вывод добавляемой строки
    printf ("\napp:  %s",app);

    // Добавление строки app в строку src1, но не более трех символов
    ft_strncat (src1, app, 3);
    // Вывод результата
    printf ("\nsrc1: %s",src1);

    // Добавление строки app в строку src2, но не более семи символов
    strncat (src2, app,7);
    // Вывод результата
    printf ("\nsrc2: %s",src2);
}

void    ft_strlcat_test()
{
    char str[100] = "unit ";
    char str1[10] = "test.@";
    printf("\nstrlcat result: %zu", ft_strlcat(str, str1, 8));
}

void	ft_putnbr_test()
{
    int num;
    num = -2147483648;
    printf("\nputnbr result: \n");
    ft_putnbr(num);
}

void	ft_putchar_fd_test()
{
    char c;
    c = 'c';
    printf("\nputchar_fd result: \n");
    ft_putchar_fd(c, 1);
}

void    ft_putstr_fd_test()
{
    char str[15] = "ebanybabay";
    printf("\nputstr_fd result: \n");
    ft_putstr_fd(str, 1);
}

void    ft_putendl_fd_test()
{
    char s[20] = "@putendl result@";
    printf("\nputendl_fd result: \n");
    ft_putendl_fd(s, 1);
}

void	ft_putnbr_fd_test()
{
    int num;
    num = 2147483647;
    printf("\nputnbr_fd result: \n");
    ft_putnbr_fd(num, 1);
}

void    ft_strclr_test()
{
    char str[5] = "str1";
    ft_strclr(str);
    printf("\nstrclr result: %s\n", str);
}

void    ft_strchr_test()
{
//    char str[5] = "str1";
//    printf("\nstrchr result: %s", ft_strchr(str, '1'));
    char *src = "there is so \0ma\0ny \0 \\0 in t\0his stri\0ng !\0\0\0\0";
    char *d1 = strchr(src, '\0');
    char *d2 = ft_strchr(src, '\0');

    if (d1 == d2)
        printf("\nTEST_SUCCESS");
    else
        printf("\nTEST_FAILED");
}

void    ft_strstr_test()
{
    char strb[100] = "MZIRIBMZIRIBMZP";
    char strlit[100] = "MZIRIBMZP";
    char strbi[100] = "MZIRIBMZIRIBMZP";
    char strlite[100] = "MZIRIBMZP";
        printf("\nft_strstr result: %s", ft_strstr(strb, strlit));
        printf("\nstrstr result: %s", strstr(strbi, strlite));
    char *s1 = "MZIRIBMZIRIBMZP";
    char *s2 = "MZIRIBMZP";
    char *i1 = strstr(s1, s2);
    char *i2 = ft_strstr(s1, s2);

    if (i1 == i2)
        printf("\nTEST_SUCCESS");
    else
        printf("\nTEST_FAILED");

}

void    ft_strnstr_test()
{
    char strb[100] = "Hello les genw";
    char strlit[10] = "Hello";
    printf("\nft_strnstr result: %s", ft_strnstr(strb, strlit, 3));
    char *s1 = "Hello les genw";
    char *s2 = "Hello";
    size_t max = strlen(s2);
    char *i1 = strnstr(s1, s2, 3);
    char *i2 = ft_strnstr(s1, s2, 3);

    if (i1 == i2)
        printf("\nSTRNSTR TEST_SUCCESS");
    else
        printf("\nTEST_FAILED");
}

void    ft_strcmp_test()
{
    char str1[10] = "long";
    char str2[10] = "long";
    printf("\nstrcmp result: %d", ft_strcmp(str1, str2));
}

void    ft_strncmp_test()
{
    char str1[10] = "R2D2";
    char str2[10] = "R2A6";
    printf("\nstrncmp result: %d", ft_strncmp(str1, str2, 2));
}

void	ft_atoi_test()
{
//    char str[25] = "9223 37";
//    printf("\natoi result: %d", ft_atoi(str));
//    printf("\nstd atoi result: %d", atoi(str));
    char *n = "99999999999999999999999999999";
    int i1 = atoi(n);
    int i2 = ft_atoi(n);

    if (i1 == i2)
        printf("\nATOI TEST_SUCCESS");
    else
        printf("\nATOI TEST_FAILED");
}

void    ft_isalpha_test()
{
    char c[5] = "a1";
    printf("\nft_isalpha: %d",ft_isalpha(c[1]));
    printf("\nisalpha: %d", isalpha(c[1]));
}

void    ft_isdigit_test()
{
    int c;
    c = 8;
    printf("\nisdigit result: %d", ft_isdigit(c));
    printf("\nstd isdigit result: %d", isdigit(c));
}

void    ft_isalnum_test()
{
    char an[5] = "a9Z0";
    printf("\nisalnum result: %d", ft_isalnum(an[1]));
    printf("\nstd isalnum result: %d", isalnum(an[1]));
}

void    ft_isascii_test()
{
    char an[5] = "a9Z0";
    printf("\nisascii result: %d", ft_isascii(an[1]));
    printf("\nstd isascii result: %d", isascii(an[1]));
}

void    ft_isprint_test()
{
    int i;
    i = 0;
    printf("\nisprint result: %d",ft_isprint(i));
    printf("\nstd isprint result: %d",isprint(i));
}

void    ft_tolower_test()
{
    int i = 0;
    char str[5] = "ABaC";
    printf("\ntolower result: ");
    while(str[i] != 0)
    {
        printf("%c", ft_tolower(str[i]));
        i++;
    }

}

void    ft_toupper_test()
{
    int i = 0;
    char str[5] = "AbaC";
    printf("\ntoupper result: ");
    while(str[i] != 0)
    {
        printf("%c", ft_toupper(str[i]));
        i++;
    }

}

void    ft_itoa_test()
{
    int num;
    num = 23;
    printf("\nitoa res: %s",ft_itoa(num));
}

void ft_putendl_test()
{
    char s[20] = "@putendl result@";
    printf("\nputendl result: \n");
    ft_putendl(s);
}

void	ft_putchar_test()
{
    char c;
    c = 'c';
    printf("\nputchar result: \n");
    ft_putchar(c);
}

void ft_memccpy_test()
{
    char src[15] = "memccpy";
    char dst[100];

    ft_bzero(dst, 100);
    ft_memccpy(dst, src, 'c', 9);
    printf("\nmemccpy result: %s", dst);
}

void ft_memmove_test()
{
    char str1[40], str2[40];
    strcpy(str1, "Born to code in C/C++.");
    ft_memmove(str2, str1, ft_strlen(str1));
    printf("\nmemmove result: %s", str2);

}

void ft_memchr_test()
{
    unsigned char src[15]= "1234567890";
    printf ("\nmemchr result: %p", ft_memchr(src, '5', 6));

    // Исходный массив
    unsigned char src1[15]="1234567890";
    // Переменная, в которую будет сохранен указатель
    // на искомый символ.
    char *sym;

    // Вывод исходного массива
    printf ("\nsrc old: %s",src1);

    // Поиск требуемого символа
    sym = ft_memchr (src1, '5', 10);

    // Если требуемый символ найден, то заменяем его
    // на символ '!'
    if (sym != NULL)
        sym[0]='!';

    // Вывод исходного массива
    printf ("\nsrc new: %s",src1);

}

void ft_memcmp_test()
{
    // Исходные массивы
    unsigned char src[15]="1234567890";
    unsigned char dst[15]="1234567890";

    // Сравниваем первые 10 байт массивов
    // и результат выводим на экран
    if (ft_memcmp (src, dst, 10) == 0)
        puts ("\nmemcmp test; Области памяти идентичные.");
    else
    puts ("\nmemcmp test; Области памяти отличаются.");
}

void ft_strncpy_test()
{
    char    src[4] = "01-";
    char    dst[10];
    char    s[4] = "01-";
    char    d[10];

    printf("\nstrNcpy result: %s", ft_strncpy(dst, src, 5));
    printf("\nstrNcpy stdres: %s", strncpy(d, s, 5));
}

void ft_strrchr_test()
{
   char str1[15] = "Hello je tesx";
    printf("\nSTDstrrchr result: %s", strrchr(str1, 't'));
    printf("\nstrrchr result: %s", ft_strrchr(str1, 't'));
}

void ft_memalloc_test()
{
    printf("\nmemalloc test: OK");
}

void ft_memdel_test()
{
    printf("\nmemdel test: memory is free");
}

void ft_strnew_test()
{
    printf("\nstrnew test: %s", ft_strnew(10));
}

void ft_strdel_test()
{
    printf("\nstrdel test: string is free");
}

void ft_striter_test()
{
    char str[10] = "abcde";
    printf("\nstriter result: \n");
    //ft_striter(str, ft_putstr);
}

void ft_striteri_test()
{
    char str[10] = "abcde";
    printf("\nstriteri result: \n");
    //ft_striteri(str, ft_putstr);
}

void ft_strmap_test()
{
    printf("\nstrmap result: \n");
}

void ft_strmapi_test()
{
    printf("\nstrmapi result: \n");
}

void ft_strequ_test()
{
    char str[10] = "unit";
    char str1[10]= "unit";
    printf("\nstrequ result: %d", ft_strequ(str, str1));
}

void ft_strnequ_test()
{
    char str[10] = "unit";
    char str1[10]= "uni";
    printf("\nstrnequ result: %d", ft_strnequ(str, str1, 3));
}

void ft_strjoin_test()
{
    char s1[10] = "unit";
    char s2[10] = " factory";
    printf("\nstrjoin: %s", ft_strjoin(s1, s2));
}

void ft_strtrim_test()
{
    char s1[20] = "  u  ";
    printf("\nstrtrim: %s", ft_strtrim(s1));
}

void ft_strsplit_test() {
    int i = 0;
    char s1[20] = "***unit*factory***";
    char **str = ft_strsplit(s1, '*');
    while (*str)
    {
        printf("\nstrsplit test: %s", *str);
        str++;
    }
}

int main()
{
    char str[3] = "OK";
//        ft_memset_test();       //  1
//            printf("%cmemset result: %s", '\n', str);
//
//        ft_bzero_test();        //  2
//            printf("%cbzero result: %s", '\n', str);
//
//        ft_memcpy_test();       //  5
//
//        ft_memccpy_test();      //
//
//        ft_memmove_test();      //
//
        ft_memchr_test();

//        ft_memcmp_test();
//
//        ft_strlen_test();       //  4
//
//        ft_strdup_test();
//
//        ft_strcpy_test();       //  3
//
//        ft_strncpy_test();
//
//        ft_strcat_test();       //  7
//
//        ft_strncat_test();
//
//        ft_strlcat_test();      // ???
//
//        ft_strchr_test();       //  12
//
        ft_strrchr_test();
//
        ft_strstr_test();       //

        ft_strnstr_test();
//
//        ft_strcmp_test();       //  14
//
//        ft_strncmp_test();
//
        ft_atoi_test();         //  13
//
//        ft_isalpha_test();
//
//        ft_isdigit_test();
//
//        ft_isalnum_test();
//
//        ft_isascii_test();
//
//        ft_isprint_test();
//
//        ft_tolower_test();      //  15
//
//        ft_toupper_test();      //  16
//
//            printf("\n---ENDLINE PART 1---\n");
//
//            ft_memalloc_test();
//            ft_memdel_test();
//            ft_strnew_test();
//            ft_strdel_test();
//            ft_strclr_test();       //  11
            ft_striter_test();
            ft_striteri_test();      //WTF
            ft_strmap_test();
            ft_strmapi_test();
//            ft_strequ_test();
//            ft_strnequ_test();
//            //ft_strsub_test();
//            ft_strjoin_test();
//            ft_strtrim_test();      // not norme
//            ft_strsplit_test();
//            ft_itoa_test();         //
//            ft_putchar_test();      //  9
//            ft_putstr_test();       //  6
//            ft_putendl_test();  //  17
//            ft_putnbr_test();       //  8
//            ft_putchar_fd_test();   //  10
//            ft_putstr_fd_test();
//            ft_putendl_fd_test();
//            ft_putnbr_fd_test();
//           printf("\n%zu",ft_length_of_digit(-2134567890));

    return 0;
}